OpenScad with python style syntax.

Combine objects using "+" and treat them like variables.


Example: Create a sphere
```python
from OpenPyCAD import Sphere
sphere = Sphere(r=5)
sphere.export(
    path_to_openscad_executable="path/to/openscad",
    export_type="stl",
)
```
