from typing import *
import os
import random
import string

TAB = " " * 4


class _Object:
    name: str = ""

    def __init__(self, *args, **kwargs):
        """
        An abstract base class for all openscad objects
        :param path_to_openscad_executable
        :param kwargs:
        """
        self.name: str = ""
        self.path_to_openscad_executable: str = ""
        self.values = {x: kwargs[x] for x in kwargs if x not in dir(self)}

    def __add__(self, other):
        return Union(self, other)

    def __and__(self, other):
        return Intersection(self, other)

    def __mul__(self, other):
        if type(other) is int:
            return Union([self for x in range(other)])

    def __sub__(self, other):
        return Difference(self, other)

    def get_text(self, indent: int = 0):
        return TAB * indent + "Base model is not compatible with OpenSCAD."

    def colorize(self, **kwargs):
        return Color(self, **kwargs)

    def mirror(self, **kwargs):
        return Mirror(self, **kwargs)

    def resize(self, **kwargs):
        return Resize(self, **kwargs)

    def rotate(self, **kwargs):
        return Rotate(self, **kwargs)

    def translate(self, **kwargs):
        return Translation(self, **kwargs)

    def process(self,
                path_to_openscad_executable: Optional[str] = None,
                name: Optional[str] = None,
                create_scad_file=True,
                view=False,
                export=False,
                export_type: Optional[str] = None,
                delete=False):
        # find openscad executable
        path_to_openscad_executable = path_to_openscad_executable or self.path_to_openscad_executable
        if not path_to_openscad_executable:
            raise ValueError(
                "No openscad executable is given. Pass it as argument either to _Model.__init__ or _Model.preview"
            )
        name = name or self.name or type(self).name
        if not name:
            # create a temporary file name
            name = str.join("", [random.choice(string.ascii_lowercase) for x in range(8)])
        if create_scad_file or view or export:  # A file is needed for every of them
            with open(name.rstrip(".scad") + ".scad", "w") as f:
                f.write(self.get_text())
        if view:
            os.system(f"{os.path.expanduser(path_to_openscad_executable)} {name.rstrip('.scad') + '.scad'}")
        if export:
            if not export_type:
                raise ValueError("Please specify your desired output file type via export_type")
            os.system(
                " ".join(
                    [
                        os.path.expanduser(path_to_openscad_executable),
                        name.rstrip(".scad") + ".scad",
                        "-o" + " " + name.rstrip(".scad") + "." + export_type.lstrip("."),
                    ]
                )
            )
        if delete:
            for x in ["scad", (export_type or "").lstrip(".")]:
                if x:
                    os.remove(".".join([name, x]))

    def write(self,
              path_to_openscad_executable: Optional[str] = None,
              name: Optional[str] = None,
              *args, **kwargs):
        self.process(
            path_to_openscad_executable=path_to_openscad_executable,
            name=name,
            create_scad_file=True,
            *args, **kwargs
        )

    def preview(self,
                path_to_openscad_executable: Optional[str] = None,
                name: Optional[str] = None,
                *args, **kwargs):
        self.process(
            path_to_openscad_executable=path_to_openscad_executable,
            name=name,
            view=True,
            *args, **kwargs
        )

    def export(self,
               path_to_openscad_executable: Optional[str] = None,
               name: Optional[str] = None,
               export_type: Optional[str] = None,
               *args, **kwargs):
        self.process(
            path_to_openscad_executable=path_to_openscad_executable,
            name=name,
            export=True,
            export_type=export_type,
            *args, **kwargs
        )


class _Model(_Object):
    name = "model"

    def get_text(self, indent: int = 0):
        attributes = self.values
        attributes_text = [key + " = "
                           + ("\"" if type(attributes[key]) is str else "")
                           + str(attributes[key]).lower()
                           + ("\"" if type(attributes[key]) is str else "")
                           for key in attributes]
        text = TAB * indent + type(self).name + "(" + ", ".join(attributes_text) + ");"
        return text


class Circle(_Model):
    name = "circle"

    def __init__(self, *args, **kwargs):
        """
        > circle(radius | d=diameter)
        :param r: [float, int]
        :param d: [float, int]
        """
        super().__init__(*args, **kwargs)


class Square(_Model):
    name = "square"

    def __init__(self, *args, **kwargs):
        """
        > square(size,center)
        > square([width,height],center)
        :param size: List[float, int]
        """
        super().__init__(*args, **kwargs)


class Polygon(_Model):
    name = "polygon"

    def __init__(self, *args, **kwargs):
        """
        > polygon([points])
        > polygon([points],[paths])
        :param points: List[List[float, int]]
        :param convexity: int
        """
        super().__init__(*args, **kwargs)


class Text(_Model):
    name = "text"

    def __init__(self, *args, **kwargs):
        """
        > text(t, size, font, halign, valign, spacing, direction, language, script)
        :param t: str
        """
        super().__init__(*args, **kwargs)


class Projection(_Model):
    name = "projection"

    def __init__(self, *args, **kwargs):
        """
        > projection(cut)
        :param cut: bool
        """
        super().__init__(*args, **kwargs)


class Cube(_Model):
    name = "cube"

    def __init__(self, *args, **kwargs):
        """
        > cube(size, center)
        > cube([width,depth,height], center)
        :param size: [int, List[int]]
        """
        super().__init__(*args, **kwargs)


class Cylinder(_Model):
    name = "cylinder"

    def __init__(self, *args, **kwargs):
        """
        > cylinder(h,r|d,center)
        > cylinder(h,r1|d1,r2|d2,center)
        :param r: [float, int]
        :param h: [float, int]
        """
        super().__init__(*args, **kwargs)


class Sphere(_Model):
    name = "sphere"

    def __init__(self, *args, **kwargs):
        """
        > sphere(radius | d=diameter)
        :param r: [float, int]
        """
        super().__init__(*args, **kwargs)


class Polyhedron(_Model):
    name = "polyhedron"

    def __init__(self, *args, **kwargs):
        """
        > polyhedron(points, faces, convexity)
        :param points: List[List[float, int]]
        """
        super().__init__(*args, **kwargs)


class Import(_Model):
    name = "import"

    def __init__(self, *args, **kwargs):
        """
        > import("….ext")
        :param file: str
        """
        super().__init__(*args, **kwargs)


class _ContainerObject(_Object):
    name = None

    def __init__(self, *args, **kwargs):
        """
        :param name: str
        """
        super().__init__(**kwargs)
        self.elements = list(args)
        for x in self.elements:
            if "name" in dir(x) and x.name:
                self.name = x.name

    def get_text(self, indent: int = 0):
        text = TAB * indent
        text = text + type(self).name + "(){\n" \
            + "\n".join([x.get_text(indent=indent + 1) for x in self.elements]) \
            + "\n" + TAB * indent + "};"
        return text


class Difference(_ContainerObject):
    name = "difference"


class Intersection(_ContainerObject):
    name = "intersection"


class Union(_ContainerObject):
    name = "union"


class _Modification(_Object):
    name = "modification"

    def __init__(self, obj, **kwargs):
        super().__init__(**kwargs)
        self.obj = obj
        self.name = obj.name

    def get_text(self, indent: int = 0):
        attributes = {x: self.values[x] for x in self.values if x != "obj"}
        attributes_text = [key + " = " + str(attributes[key]).lower() for key in attributes]
        text = TAB * indent + type(self).name + "(" + ", ".join(attributes_text) + ")" \
            + "{\n" + self.obj.get_text(indent + 1) \
            + "\n" + TAB * indent + "};"
        return text


class LinearExtrude(_Modification):
    name = "linear_extrude"

    def __init__(self, *args, **kwargs):
        """
        > linear_extrude(height,center,convexity,twist,slices)
        > linear_extrude(height = fanwidth, center = true, convexity = 10, twist = -fanrot, slices = 20, scale = 1.0, $fn = 16) {...}
        """
        super().__init__(*args, **kwargs)


class RotateExtrude(_Modification):
    name = "rotate_extrude"

    def __init__(self, *args, **kwargs):
        """
        > rotate_extrude(angle,convexity)
        > rotate_extrude(angle = 360, convexity = 2) {...}
        """
        super().__init__(*args, **kwargs)


class Translation(_Modification):
    name = "translate"

    def __init__(self, *args, **kwargs):
        """
        translate([x,y,z])
        :param obj
        :param v: List[int]
        """
        super().__init__(*args, **kwargs)


class Scale(_Modification):
    name = "scale"

    def __init__(self, *args, **kwargs):
        """
        scale([x,y,z])
        :param obj
        :param newsize: [float, int, List[float, int]]
        :param auto: bool
        """
        super().__init__(*args, **kwargs)


class Resize(_Modification):
    name = "resize"

    def __init__(self, *args, **kwargs):
        """
        resize([x,y,z],auto)
        :param obj
        :param newsize: [float, int, List[float, int]]
        :param auto: bool
        """
        super().__init__(*args, **kwargs)


class Rotate(_Modification):
    name = "rotate"

    def __init__(self, *args, **kwargs):
        """
        rotate([x,y,z])
        rotate(a, [x,y,z])
        :param obj
        :param a: [float, int, List[float, int]]
        :param v: Optional[List[float, int]]
        """
        super().__init__(*args, **kwargs)


class Mirror(_Modification):
    name = "mirror"

    def __init__(self, *args, **kwargs):
        """
        mirror([x,y,z])
        :param obj
        :param v: List[float, int]
        """
        super().__init__(*args, **kwargs)


class Color(_Modification):
    name = "color"

    def __init__(self, *args, **kwargs):
        """
        color("colorname",alpha)
        color("#hexvalue")
        color([r,g,b,a])
        :param obj
        :param c: List[float]
        """
        super().__init__(*args, **kwargs)


class Offset(_Modification):
    name = "offset"

    def __init__(self, *args, **kwargs):
        """
        offset(r|delta,chamfer)
        :param obj
        :param r: [float, int]
        :param chamfer,
        """
        super().__init__(*args, **kwargs)


class Hull(_Modification):
    name = "hull"

    def __init__(self, *args, **kwargs):
        """
        hull()
        :param obj
        """
        super().__init__(*args, **kwargs)


class Minkowski(_Modification):
    name = "minkowski"

    def __init__(self, *args, **kwargs):
        """
        minkowski()
        :param obj
        """
        super().__init__(*args, **kwargs)
