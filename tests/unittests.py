import unittest
from OpenPyCAD import *


class Test(unittest.TestCase):
    def test_cube(self):
        obj = Cube(v=[1, 1, 1])
        res = "cube(v = [1, 1, 1]);"
        assert obj.get_text() == res, obj.get_text()

    def test_cylinder(self):
        obj = Cylinder(v=[1, 1, 1])
        res = "cylinder(v = [1, 1, 1]);"
        assert obj.get_text() == res, obj.get_text()

    def test_sphere(self):
        obj = Sphere(r=1)
        res = "sphere(r = 1);"
        assert obj.get_text() == res, obj.get_text()

    def test_import(self):
        """
        For import testing take a look at the integration tests.
        """
        return

    def test_union(self):
        obj = Cube(size=5) + Cube(size=5)
        res = "union(){\n    cube(size = 5);\n    cube(size = 5);\n};"
        assert obj.get_text() == res, obj.get_text()

    def test_difference(self):
        obj = Cube(size=5) - Cube(size=5)
        res = "difference(){\n    cube(size = 5);\n    cube(size = 5);\n};"
        assert obj.get_text() == res, obj.get_text()

    def test_intersection(self):
        obj = Cube(size=5) & Cube(size=5)
        res = "intersection(){\n    cube(size = 5);\n    cube(size = 5);\n};"
        assert obj.get_text() == res, obj.get_text()

    def test_translation(self):
        v = [1, 2, 3]
        obj1 = Cube(size=5).translate(v=v)
        obj2 = Translation(obj=Cube(size=5), v=v)
        res = "translate(v = [1, 2, 3]){\n    cube(size = 5);\n};"
        assert obj1.get_text() == res, obj1.get_text()
        assert obj2.get_text() == res, obj2.get_text()

    def test_resize(self):
        v = [1, 2, 3]
        obj1 = Cube(size=5).resize(newsize=v)
        obj2 = Resize(obj=Cube(size=5), newsize=v)
        res = "resize(newsize = [1, 2, 3]){\n    cube(size = 5);\n};"
        assert obj1.get_text() == res, obj1.get_text()
        assert obj2.get_text() == res, obj2.get_text()

    def test_rotate(self):
        v = [1, 2, 3]
        obj1 = Cube(size=5).rotate(a=v)
        obj2 = Rotate(obj=Cube(size=5), a=v)
        res = "rotate(a = [1, 2, 3]){\n    cube(size = 5);\n};"
        assert obj1.get_text() == res, obj1.get_text()
        assert obj2.get_text() == res, obj2.get_text()

    def test_mirror(self):
        v = [1, 2, 3]
        obj1 = Cube(size=5).mirror(v=v)
        obj2 = Mirror(obj=Cube(size=5), v=v)
        res = "mirror(v = [1, 2, 3]){\n    cube(size = 5);\n};"
        assert obj1.get_text() == res, obj1.get_text()
        assert obj2.get_text() == res, obj2.get_text()

    def test_color(self):
        v = [1, .5, 0, 1]
        obj1 = Cube(size=5).colorize(c=v)
        obj2 = Color(obj=Cube(size=5), c=v)
        res = "color(c = [1, 0.5, 0, 1]){\n    cube(size = 5);\n};"
        assert obj1.get_text() == res, obj1.get_text()
        assert obj2.get_text() == res, obj2.get_text()


unittest.main()
