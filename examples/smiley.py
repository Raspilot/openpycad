"""
This exmaple demonstrates how to model a 2d smiley.
"""

from OpenPyCAD import *

# Create a round base
face = Cylinder(r=25, h=5, center=True)

# Create the eyes
eyeball = Cylinder(r=2.5, h=10, center=True, **{"$fn": 36}).translate(v=[-7.5, 7.5, 0])
eyes = eyeball + Mirror(eyeball, v=[0, 1, 0])

# Create the mouth
mouth = Cylinder(r=20, h=10, center=True) - Cylinder(r=40, h=15, center=True).translate(v=[-25, 0, 0])
mouth = mouth.translate(v=[-2.5, 0, 0])

# Assemble the smiley
smiley = face - eyes - mouth

smiley.name = "smiley"

orange = [1, .5, 0, 1]
smiley = smiley.colorize(c=orange)


path_to_openscad = "path/to/openscad_executable"
smiley.write(path_to_openscad)
smiley.export(path_to_openscad, export_type="3mf")
smiley.export(path_to_openscad, export_type="stl")
smiley.preview(path_to_openscad)
