union(){
    union(){
        union(){
            color(c = [1, 1, 1, 1]){
                sphere(r = 50);
            };
            translate(v = [0, 0, 60]){
                color(c = [1, 1, 1, 1]){
                    sphere(r = 40);
                };
            };
        };
        translate(v = [0, 0, 110]){
            color(c = [1, 1, 1, 1]){
                sphere(r = 30);
            };
        };
    };
    color(c = [0, 0, 0, 1]){
        translate(v = [0, 0, 130]){
            union(){
                cylinder(r = 40, h = 5, center = true);
                cylinder(r = 20, h = 30);
            };
        };
    };
};