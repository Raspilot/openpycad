from OpenPyCAD import Sphere, Cylinder
path_to_openscad = "~/Programs/openscad/openscad/OpenSCAD-2019.05-x86_64.AppImage"


ball1 = Sphere(r=50).colorize(c=[1, 1, 1, 1])
ball2 = Sphere(r=40).colorize(c=[1, 1, 1, 1]).translate(v=[0, 0, 60])
ball3 = Sphere(r=30).colorize(c=[1, 1, 1, 1]).translate(v=[0, 0, 110])

body = ball1 + ball2 + ball3

hat = Cylinder(r=40, h=5, center=True) + Cylinder(r=20, h=30)
hat = hat.translate(v=[0, 0, 130])

snowman = body + hat.colorize(c=[0, 0, 0, 1])
snowman.name = "snowman"
snowman.export(path_to_openscad_executable=path_to_openscad, export_type="stl")
snowman.export(path_to_openscad_executable=path_to_openscad, export_type="3mf")
