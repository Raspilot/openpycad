color(c = [1, 0.5, 0, 1]){
    difference(){
        difference(){
            cylinder(r = 25, h = 5, center = true);
            union(){
                translate(v = [-7.5, 7.5, 0]){
                    cylinder(r = 2.5, h = 10, center = true, $fn = 36);
                };
                mirror(v = [0, 1, 0]){
                    translate(v = [-7.5, 7.5, 0]){
                        cylinder(r = 2.5, h = 10, center = true, $fn = 36);
                    };
                };
            };
        };
        translate(v = [-2.5, 0, 0]){
            difference(){
                cylinder(r = 20, h = 10, center = true);
                translate(v = [-25, 0, 0]){
                    cylinder(r = 40, h = 15, center = true);
                };
            };
        };
    };
};